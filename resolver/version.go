package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

func GetVersion(ctx context.Context, args struct{ ID graphql.ID }) (*VersionResolver, error) {
	conn := database.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		var resolver *VersionResolver
		return resolver, err
	}

	// Need to handle record not found
	version := &model.Version{}
	err = conn.First(version, id).Error
	if err != nil {
		var resolver *VersionResolver
		if database.DB.IsRecordNotFound(err) {
			return resolver, notFoundError{
				Code:    "Not found",
				Message: "This version could not be found",
			}
		}
		return resolver, err
	}

	resolver := &VersionResolver{
		version: version,
	}

	return resolver, nil
}

// Type returns either a single pokemon
func (r QueryResolver) Version(ctx context.Context, args struct{ ID graphql.ID }) (*VersionResolver, error) {
	return GetVersion(ctx, args)
}

// Types returns a list of all pokemon species
func (r QueryResolver) Versions(ctx context.Context) ([]*VersionResolver, error) {
	conn := r.DB.Conn

	// Need to handle record not found
	var versions []*model.Version
	err := conn.Find(&versions).Error
	if err != nil {
		var resolver []*VersionResolver
		if r.DB.IsRecordNotFound(err) {
			return resolver, notFoundError{
				Code:    "Not found",
				Message: "No versions could not be found",
			}
		}
		return resolver, err
	}

	var resolver []*VersionResolver
	for index := range versions {
		newVersionResolver := &VersionResolver{
			version: versions[index],
		}
		resolver = append(resolver, newVersionResolver)
	}

	return resolver, nil
}

type VersionResolver struct {
	version *model.Version
}

func (r *VersionResolver) ID() graphql.ID {
	id := strconv.Itoa(r.version.ID)
	return graphql.ID(id)
}

func (r *VersionResolver) VersionGroupID() int32 {
	id := int32(r.version.VersionGroupID)
	return id
}

func (r *VersionResolver) Identifier() string {
	return r.version.Identifier
}
