package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

func GetPokemonType(ctx context.Context, args struct{ ID graphql.ID }) ([]*PokemonTypeResolver, error) {
	conn := database.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		panic(err)
	}

	// Need to handle record not found
	var pokemonType []*model.PokemonType
	err = conn.Where("pokemon_id = ?", id).Find(&pokemonType).Error
	if err != nil {
		panic(err)
	}

	var resolver []*PokemonTypeResolver
	for index := range pokemonType {
		newResolver := &PokemonTypeResolver{
			pokemonType: pokemonType[index],
		}
		resolver = append(resolver, newResolver)
	}

	return resolver, nil
}

type PokemonTypeResolver struct {
	pokemonType *model.PokemonType
}

func (r *PokemonTypeResolver) Slot() int32 {
	id := int32(r.pokemonType.Slot)
	return id
}

// TypeID is not actually returned by graphql queries
func (r *PokemonTypeResolver) TypeID() graphql.ID {
	id := strconv.Itoa(r.pokemonType.TypeID)
	return graphql.ID(id)
}

func (r *PokemonTypeResolver) Type(ctx context.Context) (*TypeResolver, error) {
	grapqhlID := struct {
		ID graphql.ID
	}{
		ID: r.TypeID(),
	}
	pokemonType, err := GetType(ctx, grapqhlID)
	if err != nil {
		return &TypeResolver{}, err
	}

	return pokemonType, nil
}
