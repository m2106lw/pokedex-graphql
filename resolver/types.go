package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

func GetType(ctx context.Context, args struct{ ID graphql.ID }) (*TypeResolver, error) {
	conn := database.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		panic(err)
	}

	// Need to handle record not found
	pokemonType := &model.Type{}
	err = conn.First(pokemonType, id).Error
	if err != nil {
		panic(err)
	}

	resolver := &TypeResolver{
		pokemonType: pokemonType,
	}

	return resolver, nil
}

// Type returns either a single pokemon
func (r QueryResolver) Type(ctx context.Context, args struct{ ID graphql.ID }) (*TypeResolver, error) {
	return GetType(ctx, args)
}

// Types returns a list of all pokemon species
func (r QueryResolver) Types(ctx context.Context) ([]*TypeResolver, error) {
	conn := r.DB.Conn

	// Need to handle record not found
	var pokemonTypes []*model.Type
	err := conn.Find(&pokemonTypes).Error
	if err != nil {
		panic(err)
	}

	var resolver []*TypeResolver
	for index := range pokemonTypes {
		newTypeResolver := &TypeResolver{
			pokemonType: pokemonTypes[index],
		}
		resolver = append(resolver, newTypeResolver)
	}

	return resolver, nil
}

type TypeResolver struct {
	pokemonType *model.Type
}

func (r *TypeResolver) ID() graphql.ID {
	id := strconv.Itoa(r.pokemonType.ID)
	return graphql.ID(id)

}

func (r *TypeResolver) Identifier() string {
	return r.pokemonType.Identifier
}

func (r *TypeResolver) GenerationID() int32 {
	id := int32(r.pokemonType.GenerationID)
	return id
}

func (r *TypeResolver) DamageClassID() *int32 {
	id := int32(r.pokemonType.DamageClassID)
	return &id
}
