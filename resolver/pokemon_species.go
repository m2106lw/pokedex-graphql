package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

func GetSpecies(ctx context.Context, args struct{ ID graphql.ID }) (*SpeciesResolver, error) {
	conn := database.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		panic(err)
	}

	// Need to handle record not found
	species := &model.PokemonSpecies{}
	err = conn.First(species, id).Error
	if err != nil {
		panic(err)
	}

	resolver := &SpeciesResolver{
		species: species,
	}

	return resolver, nil
}

// Species returns either a single pokemon
func (r QueryResolver) Species(ctx context.Context, args struct{ ID graphql.ID }) (*SpeciesResolver, error) {
	return GetSpecies(ctx, args)
}

// AllSpecies returns a list of all pokemon species
func (r QueryResolver) AllSpecies(ctx context.Context) ([]*SpeciesResolver, error) {
	conn := r.DB.Conn

	// Need to handle record not found
	var allSpecies []*model.PokemonSpecies
	err := conn.Find(&allSpecies).Error
	if err != nil {
		panic(err)
	}

	var resolver []*SpeciesResolver
	for index := range allSpecies {
		newSpeciesResolver := &SpeciesResolver{
			species: allSpecies[index],
		}
		resolver = append(resolver, newSpeciesResolver)
	}

	return resolver, nil
}

type SpeciesResolver struct {
	species *model.PokemonSpecies
}

func (r *SpeciesResolver) ID() graphql.ID {
	id := strconv.Itoa(r.species.ID)
	return graphql.ID(id)

}

func (r *SpeciesResolver) Identifier() string {
	return r.species.Identifier
}

func (r *SpeciesResolver) GenerationID() *int32 {
	id := int32(r.species.GenerationID)
	return &id
}

func (r *SpeciesResolver) EvolvesFromSpeciesID() *int32 {
	id := int32(r.species.EvolvesFromSpeciesID)
	return &id
}

func (r *SpeciesResolver) EvolutionChainID() *int32 {
	id := int32(r.species.EvolutionChainID)
	return &id
}

func (r *SpeciesResolver) ColorID() int32 {
	id := int32(r.species.ColorID)
	return id
}

func (r *SpeciesResolver) ShapeID() int32 {
	id := int32(r.species.ShapeID)
	return id
}

func (r *SpeciesResolver) HabitatID() *int32 {
	id := int32(r.species.HabitatID)
	return &id
}

func (r *SpeciesResolver) GenderRate() int32 {
	id := int32(r.species.GenderRate)
	return id
}

func (r *SpeciesResolver) CaptureRate() int32 {
	id := int32(r.species.CaptureRate)
	return id
}

func (r *SpeciesResolver) BaseHappiness() int32 {
	id := int32(r.species.BaseHappiness)
	return id
}

func (r *SpeciesResolver) IsBaby() bool {
	return r.species.IsBaby
}

func (r *SpeciesResolver) HatchCounter() int32 {
	id := int32(r.species.HatchCounter)
	return id
}

func (r *SpeciesResolver) HasGenderDifferences() bool {
	return r.species.HasGenderDifferences
}

func (r *SpeciesResolver) GrowthRateID() int32 {
	id := int32(r.species.GrowthRateID)
	return id
}

func (r *SpeciesResolver) FormsSwitchable() bool {
	return r.species.FormsSwitchable
}

func (r *SpeciesResolver) Order() int32 {
	id := int32(r.species.Order)
	return id
}

func (r *SpeciesResolver) ConquestOrder() *int32 {
	id := int32(r.species.ConquestOrder)
	return &id
}
