package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

// Pokemon returns either a single pokemon
func (r QueryResolver) Pokemon(ctx context.Context, args struct{ ID graphql.ID }) (*PokemonResolver, error) {
	conn := r.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		var resolver *PokemonResolver
		return resolver, err
	}

	// Need to handle record not found
	pokemon := &model.Pokemon{}
	err = conn.First(pokemon, id).Error
	if err != nil {
		var resolver *PokemonResolver
		if r.DB.IsRecordNotFound(err) {
			return resolver, notFoundError{
				Code:    "Not found",
				Message: "This pokemon could not be found",
			}
		}
		return resolver, err
	}

	resolver := &PokemonResolver{
		pokemon: pokemon,
	}

	return resolver, nil
}

// allPokemon returns a list of all pokemon (large)
func (r QueryResolver) AllPokemon(ctx context.Context) ([]*PokemonResolver, error) {
	conn := r.DB.Conn

	// Need to handle record not found
	var allPokemon []*model.Pokemon
	err := conn.Find(&allPokemon).Error
	if err != nil {
		var resolver []*PokemonResolver
		return resolver, err
	}

	var resolver []*PokemonResolver
	for index := range allPokemon {
		newPokemonResolver := &PokemonResolver{
			pokemon: allPokemon[index],
		}
		resolver = append(resolver, newPokemonResolver)
	}

	return resolver, nil
}

type PokemonResolver struct {
	pokemon *model.Pokemon
}

func (r *PokemonResolver) ID() graphql.ID {
	id := strconv.Itoa(r.pokemon.ID)
	return graphql.ID(id)
}

func (r *PokemonResolver) Identifier() string {
	return r.pokemon.Identifier
}

// TODO: Look into data loaders?
func (r *PokemonResolver) Species(ctx context.Context) (*SpeciesResolver, error) {
	grapqhlID := struct {
		ID graphql.ID
	}{
		ID: r.ID(),
	}
	species, err := GetSpecies(ctx, grapqhlID)
	if err != nil {
		return &SpeciesResolver{}, err
	}
	return species, nil
}

func (r *PokemonResolver) Types(ctx context.Context) ([]*PokemonTypeResolver, error) {
	grapqhlID := struct {
		ID graphql.ID
	}{
		ID: r.ID(),
	}
	types, err := GetPokemonType(ctx, grapqhlID)
	if err != nil {
		var emptySlice []*PokemonTypeResolver
		return emptySlice, err
	}
	return types, nil
}

func (r *PokemonResolver) Height() int32 {
	id := int32(r.pokemon.Height)
	return id
}

func (r *PokemonResolver) Weight() int32 {
	id := int32(r.pokemon.Weight)
	return id
}

func (r *PokemonResolver) BaseExperience() int32 {
	id := int32(r.pokemon.BaseExperience)
	return id
}

func (r *PokemonResolver) Order() int32 {
	id := int32(r.pokemon.Order)
	return id
}

func (r *PokemonResolver) IsDefault() bool {
	return r.pokemon.IsDefault
}

func (r *PokemonResolver) Versions(ctx context.Context) ([]*PokemonGameIndexResolver, error) {
	grapqhlID := struct {
		ID graphql.ID
	}{
		ID: r.ID(),
	}
	versions, err := GetPokemonGameVersion(ctx, grapqhlID)
	if err != nil {
		var emptySlice []*PokemonGameIndexResolver
		return emptySlice, err
	}
	return versions, nil
}
