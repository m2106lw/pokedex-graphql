package resolver

import (
	"context"
	"strconv"

	graphql "github.com/graph-gophers/graphql-go"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/model"
)

func GetPokemonGameVersion(ctx context.Context, args struct{ ID graphql.ID }) ([]*PokemonGameIndexResolver, error) {
	conn := database.DB.Conn

	id, err := strconv.Atoi(string(args.ID))
	if err != nil {
		panic(err)
	}

	// Need to handle record not found
	var pokemonGameIndecies []*model.PokemonGameIndex
	err = conn.Where("pokemon_id = ?", id).Find(&pokemonGameIndecies).Error
	if err != nil {
		panic(err)
	}

	var resolver []*PokemonGameIndexResolver
	for index := range pokemonGameIndecies {
		newResolver := &PokemonGameIndexResolver{
			pokemonGameIndex: pokemonGameIndecies[index],
		}
		resolver = append(resolver, newResolver)
	}

	return resolver, nil
}

type PokemonGameIndexResolver struct {
	pokemonGameIndex *model.PokemonGameIndex
}

func (r *PokemonGameIndexResolver) GameIndex() int32 {
	id := int32(r.pokemonGameIndex.GameIndex)
	return id
}

// VersionID is not actually returned by graphql queries
func (r *PokemonGameIndexResolver) VersionID() graphql.ID {
	id := strconv.Itoa(r.pokemonGameIndex.VersionID)
	return graphql.ID(id)
}

func (r *PokemonGameIndexResolver) Version(ctx context.Context) (*VersionResolver, error) {
	grapqhlID := struct {
		ID graphql.ID
	}{
		ID: r.VersionID(),
	}
	pokemonVersion, err := GetVersion(ctx, grapqhlID)
	if err != nil {
		return &VersionResolver{}, err
	}

	return pokemonVersion, nil
}
