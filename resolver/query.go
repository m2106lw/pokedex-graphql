package resolver

import (
	"fmt"

	"gitlab.com/m2106lw/pokedex-graphql/database"
)

type QueryResolver struct {
	DB *database.DBInfo
}

func NewRoot(db *database.DBInfo) (*QueryResolver, error) {
	return &QueryResolver{DB: db}, nil
}

type ResolverError interface {
	error
	Extensions() map[string]interface{}
}

type notFoundError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (e notFoundError) Error() string {
	return fmt.Sprintf("error [%s}: %s", e.Code, e.Message)
}

func (e notFoundError) Extensions() map[string]interface{} {
	return map[string]interface{}{
		"code":    e.Code,
		"message": e.Message,
	}
}
