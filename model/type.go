package model

type Type struct {
	ID            int    `gorm:"column:id;primary_key" json:"id"`
	Identifier    string `gorm:"column:identifier;not null" json:"identifier"`
	GenerationID  int    `gorm:"column:generation_id;not null" json:"generation_id"`
	DamageClassID int    `gorm:"column:damage_class_id" json:"damage_class_id"`
}

// TableName sets the insert table name for this struct type
func (t *Type) TableName() string {
	return "types"
}
