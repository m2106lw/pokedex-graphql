package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ContestTypeName struct {
	ContestTypeID   int    `gorm:"column:contest_type_id;primary_key" json:"contest_type_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
	Flavor          string `gorm:"column:flavor" json:"flavor"`
	Color           string `gorm:"column:color" json:"color"`
}

// TableName sets the insert table name for this struct type
func (c *ContestTypeName) TableName() string {
	return "contest_type_names"
}
