package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemCategory struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	PocketID   int    `gorm:"column:pocket_id" json:"pocket_id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (i *ItemCategory) TableName() string {
	return "item_categories"
}
