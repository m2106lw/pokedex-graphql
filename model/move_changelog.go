package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveChangelog struct {
	MoveID                  int `gorm:"column:move_id;primary_key" json:"move_id"`
	ChangedInVersionGroupID int `gorm:"column:changed_in_version_group_id" json:"changed_in_version_group_id"`
	TypeID                  int `gorm:"column:type_id" json:"type_id"`
	Power                   int `gorm:"column:power" json:"power"`
	Pp                      int `gorm:"column:pp" json:"pp"`
	Accuracy                int `gorm:"column:accuracy" json:"accuracy"`
	Priority                int `gorm:"column:priority" json:"priority"`
	TargetID                int `gorm:"column:target_id" json:"target_id"`
	EffectID                int `gorm:"column:effect_id" json:"effect_id"`
	EffectChance            int `gorm:"column:effect_chance" json:"effect_chance"`
}

// TableName sets the insert table name for this struct type
func (m *MoveChangelog) TableName() string {
	return "move_changelog"
}
