package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonItem struct {
	PokemonID int `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	VersionID int `gorm:"column:version_id" json:"version_id"`
	ItemID    int `gorm:"column:item_id" json:"item_id"`
	Rarity    int `gorm:"column:rarity" json:"rarity"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonItem) TableName() string {
	return "pokemon_items"
}
