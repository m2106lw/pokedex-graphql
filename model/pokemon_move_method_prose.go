package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonMoveMethodProse struct {
	PokemonMoveMethodID int    `gorm:"column:pokemon_move_method_id;primary_key" json:"pokemon_move_method_id"`
	LocalLanguageID     int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name                string `gorm:"column:name" json:"name"`
	Description         string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonMoveMethodProse) TableName() string {
	return "pokemon_move_method_prose"
}
