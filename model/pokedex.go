package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Pokedex struct {
	ID           int    `gorm:"column:id;primary_key" json:"id"`
	RegionID     int    `gorm:"column:region_id" json:"region_id"`
	Identifier   string `gorm:"column:identifier" json:"identifier"`
	IsMainSeries bool   `gorm:"column:is_main_series" json:"is_main_series"`
}

// TableName sets the insert table name for this struct type
func (p *Pokedex) TableName() string {
	return "pokedexes"
}
