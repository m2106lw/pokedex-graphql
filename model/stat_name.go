package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type StatName struct {
	StatID          int    `gorm:"column:stat_id;primary_key" json:"stat_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (s *StatName) TableName() string {
	return "stat_names"
}
