package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemFlagMap struct {
	ItemID     int `gorm:"column:item_id;primary_key" json:"item_id"`
	ItemFlagID int `gorm:"column:item_flag_id" json:"item_flag_id"`
}

// TableName sets the insert table name for this struct type
func (i *ItemFlagMap) TableName() string {
	return "item_flag_map"
}
