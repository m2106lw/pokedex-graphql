package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveMetum struct {
	MoveID         int `gorm:"column:move_id;primary_key" json:"move_id"`
	MetaCategoryID int `gorm:"column:meta_category_id" json:"meta_category_id"`
	MetaAilmentID  int `gorm:"column:meta_ailment_id" json:"meta_ailment_id"`
	MinHits        int `gorm:"column:min_hits" json:"min_hits"`
	MaxHits        int `gorm:"column:max_hits" json:"max_hits"`
	MinTurns       int `gorm:"column:min_turns" json:"min_turns"`
	MaxTurns       int `gorm:"column:max_turns" json:"max_turns"`
	Drain          int `gorm:"column:drain" json:"drain"`
	Healing        int `gorm:"column:healing" json:"healing"`
	CritRate       int `gorm:"column:crit_rate" json:"crit_rate"`
	AilmentChance  int `gorm:"column:ailment_chance" json:"ailment_chance"`
	FlinchChance   int `gorm:"column:flinch_chance" json:"flinch_chance"`
	StatChance     int `gorm:"column:stat_chance" json:"stat_chance"`
}

// TableName sets the insert table name for this struct type
func (m *MoveMetum) TableName() string {
	return "move_meta"
}
