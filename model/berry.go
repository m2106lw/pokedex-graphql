package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Berry struct {
	ID                int `gorm:"column:id;primary_key" json:"id"`
	ItemID            int `gorm:"column:item_id" json:"item_id"`
	FirmnessID        int `gorm:"column:firmness_id" json:"firmness_id"`
	NaturalGiftPower  int `gorm:"column:natural_gift_power" json:"natural_gift_power"`
	NaturalGiftTypeID int `gorm:"column:natural_gift_type_id" json:"natural_gift_type_id"`
	Size              int `gorm:"column:size" json:"size"`
	MaxHarvest        int `gorm:"column:max_harvest" json:"max_harvest"`
	GrowthTime        int `gorm:"column:growth_time" json:"growth_time"`
	SoilDryness       int `gorm:"column:soil_dryness" json:"soil_dryness"`
	Smoothness        int `gorm:"column:smoothness" json:"smoothness"`
}

// TableName sets the insert table name for this struct type
func (b *Berry) TableName() string {
	return "berries"
}
