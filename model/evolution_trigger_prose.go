package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EvolutionTriggerProse struct {
	EvolutionTriggerID int    `gorm:"column:evolution_trigger_id;primary_key" json:"evolution_trigger_id"`
	LocalLanguageID    int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name               string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (e *EvolutionTriggerProse) TableName() string {
	return "evolution_trigger_prose"
}
