package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Stat struct {
	ID            int    `gorm:"column:id;primary_key" json:"id"`
	DamageClassID int    `gorm:"column:damage_class_id" json:"damage_class_id"`
	Identifier    string `gorm:"column:identifier" json:"identifier"`
	IsBattleOnly  bool   `gorm:"column:is_battle_only" json:"is_battle_only"`
	GameIndex     int    `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (s *Stat) TableName() string {
	return "stats"
}
