package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveDisplacement struct {
	ID            int    `gorm:"column:id;primary_key" json:"id"`
	Identifier    string `gorm:"column:identifier" json:"identifier"`
	AffectsTarget bool   `gorm:"column:affects_target" json:"affects_target"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveDisplacement) TableName() string {
	return "conquest_move_displacements"
}
