package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type LocationGameIndex struct {
	LocationID   int `gorm:"column:location_id;primary_key" json:"location_id"`
	GenerationID int `gorm:"column:generation_id" json:"generation_id"`
	GameIndex    int `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (l *LocationGameIndex) TableName() string {
	return "location_game_indices"
}
