package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveEffectProse struct {
	MoveEffectID    int    `gorm:"column:move_effect_id;primary_key" json:"move_effect_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	ShortEffect     string `gorm:"column:short_effect" json:"short_effect"`
	Effect          string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (m *MoveEffectProse) TableName() string {
	return "move_effect_prose"
}
