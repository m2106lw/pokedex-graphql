package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Generation struct {
	ID           int    `gorm:"column:id;primary_key" json:"id"`
	MainRegionID int    `gorm:"column:main_region_id" json:"main_region_id"`
	Identifier   string `gorm:"column:identifier" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (g *Generation) TableName() string {
	return "generations"
}
