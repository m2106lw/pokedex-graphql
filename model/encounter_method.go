package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterMethod struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	Order      int    `gorm:"column:order" json:"order"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterMethod) TableName() string {
	return "encounter_methods"
}
