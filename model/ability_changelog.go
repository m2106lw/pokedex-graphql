package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type AbilityChangelog struct {
	ID                      int `gorm:"column:id;primary_key" json:"id"`
	AbilityID               int `gorm:"column:ability_id" json:"ability_id"`
	ChangedInVersionGroupID int `gorm:"column:changed_in_version_group_id" json:"changed_in_version_group_id"`
}

// TableName sets the insert table name for this struct type
func (a *AbilityChangelog) TableName() string {
	return "ability_changelog"
}
