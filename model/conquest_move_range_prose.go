package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveRangeProse struct {
	ConquestMoveRangeID int    `gorm:"column:conquest_move_range_id;primary_key" json:"conquest_move_range_id"`
	LocalLanguageID     int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name                string `gorm:"column:name" json:"name"`
	Description         string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveRangeProse) TableName() string {
	return "conquest_move_range_prose"
}
