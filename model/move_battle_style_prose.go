package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveBattleStyleProse struct {
	MoveBattleStyleID int    `gorm:"column:move_battle_style_id;primary_key" json:"move_battle_style_id"`
	LocalLanguageID   int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name              string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (m *MoveBattleStyleProse) TableName() string {
	return "move_battle_style_prose"
}
