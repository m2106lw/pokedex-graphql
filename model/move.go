package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Move struct {
	ID                   int    `gorm:"column:id;primary_key" json:"id"`
	Identifier           string `gorm:"column:identifier" json:"identifier"`
	GenerationID         int    `gorm:"column:generation_id" json:"generation_id"`
	TypeID               int    `gorm:"column:type_id" json:"type_id"`
	Power                int    `gorm:"column:power" json:"power"`
	Pp                   int    `gorm:"column:pp" json:"pp"`
	Accuracy             int    `gorm:"column:accuracy" json:"accuracy"`
	Priority             int    `gorm:"column:priority" json:"priority"`
	TargetID             int    `gorm:"column:target_id" json:"target_id"`
	DamageClassID        int    `gorm:"column:damage_class_id" json:"damage_class_id"`
	EffectID             int    `gorm:"column:effect_id" json:"effect_id"`
	EffectChance         int    `gorm:"column:effect_chance" json:"effect_chance"`
	ContestTypeID        int    `gorm:"column:contest_type_id" json:"contest_type_id"`
	ContestEffectID      int    `gorm:"column:contest_effect_id" json:"contest_effect_id"`
	SuperContestEffectID int    `gorm:"column:super_contest_effect_id" json:"super_contest_effect_id"`
}

// TableName sets the insert table name for this struct type
func (m *Move) TableName() string {
	return "moves"
}
