package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveFlavorSummary struct {
	MoveID          int    `gorm:"column:move_id;primary_key" json:"move_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	FlavorSummary   string `gorm:"column:flavor_summary" json:"flavor_summary"`
}

// TableName sets the insert table name for this struct type
func (m *MoveFlavorSummary) TableName() string {
	return "move_flavor_summaries"
}
