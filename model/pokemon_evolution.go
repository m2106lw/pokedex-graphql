package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonEvolution struct {
	ID                    int  `gorm:"column:id;primary_key" json:"id"`
	EvolvedSpeciesID      int  `gorm:"column:evolved_species_id" json:"evolved_species_id"`
	EvolutionTriggerID    int  `gorm:"column:evolution_trigger_id" json:"evolution_trigger_id"`
	TriggerItemID         int  `gorm:"column:trigger_item_id" json:"trigger_item_id"`
	MinimumLevel          int  `gorm:"column:minimum_level" json:"minimum_level"`
	GenderID              int  `gorm:"column:gender_id" json:"gender_id"`
	LocationID            int  `gorm:"column:location_id" json:"location_id"`
	HeldItemID            int  `gorm:"column:held_item_id" json:"held_item_id"`
	KnownMoveID           int  `gorm:"column:known_move_id" json:"known_move_id"`
	KnownMoveTypeID       int  `gorm:"column:known_move_type_id" json:"known_move_type_id"`
	MinimumHappiness      int  `gorm:"column:minimum_happiness" json:"minimum_happiness"`
	MinimumBeauty         int  `gorm:"column:minimum_beauty" json:"minimum_beauty"`
	MinimumAffection      int  `gorm:"column:minimum_affection" json:"minimum_affection"`
	RelativePhysicalStats int  `gorm:"column:relative_physical_stats" json:"relative_physical_stats"`
	PartySpeciesID        int  `gorm:"column:party_species_id" json:"party_species_id"`
	PartyTypeID           int  `gorm:"column:party_type_id" json:"party_type_id"`
	TradeSpeciesID        int  `gorm:"column:trade_species_id" json:"trade_species_id"`
	NeedsOverworldRain    bool `gorm:"column:needs_overworld_rain" json:"needs_overworld_rain"`
	TurnUpsideDown        bool `gorm:"column:turn_upside_down" json:"turn_upside_down"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonEvolution) TableName() string {
	return "pokemon_evolution"
}
