package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type AbilityName struct {
	AbilityID       int    `gorm:"column:ability_id;primary_key" json:"ability_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (a *AbilityName) TableName() string {
	return "ability_names"
}
