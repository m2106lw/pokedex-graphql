package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type CharacteristicText struct {
	CharacteristicID int    `gorm:"column:characteristic_id;primary_key" json:"characteristic_id"`
	LocalLanguageID  int    `gorm:"column:local_language_id" json:"local_language_id"`
	Message          string `gorm:"column:message" json:"message"`
}

// TableName sets the insert table name for this struct type
func (c *CharacteristicText) TableName() string {
	return "characteristic_text"
}
