package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Location struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	RegionID   int    `gorm:"column:region_id" json:"region_id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (l *Location) TableName() string {
	return "locations"
}
