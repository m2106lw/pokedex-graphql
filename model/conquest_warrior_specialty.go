package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorSpecialty struct {
	WarriorID int `gorm:"column:warrior_id;primary_key" json:"warrior_id"`
	TypeID    int `gorm:"column:type_id" json:"type_id"`
	Slot      int `gorm:"column:slot" json:"slot"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorSpecialty) TableName() string {
	return "conquest_warrior_specialties"
}
