package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveMetaStatChange struct {
	MoveID int `gorm:"column:move_id;primary_key" json:"move_id"`
	StatID int `gorm:"column:stat_id" json:"stat_id"`
	Change int `gorm:"column:change" json:"change"`
}

// TableName sets the insert table name for this struct type
func (m *MoveMetaStatChange) TableName() string {
	return "move_meta_stat_changes"
}
