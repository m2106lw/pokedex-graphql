package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type NaturePokeathlonStat struct {
	NatureID         int `gorm:"column:nature_id;primary_key" json:"nature_id"`
	PokeathlonStatID int `gorm:"column:pokeathlon_stat_id" json:"pokeathlon_stat_id"`
	MaxChange        int `gorm:"column:max_change" json:"max_change"`
}

// TableName sets the insert table name for this struct type
func (n *NaturePokeathlonStat) TableName() string {
	return "nature_pokeathlon_stats"
}
