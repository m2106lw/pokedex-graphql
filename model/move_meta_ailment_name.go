package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveMetaAilmentName struct {
	MoveMetaAilmentID int    `gorm:"column:move_meta_ailment_id;primary_key" json:"move_meta_ailment_id"`
	LocalLanguageID   int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name              string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (m *MoveMetaAilmentName) TableName() string {
	return "move_meta_ailment_names"
}
