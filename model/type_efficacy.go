package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type TypeEfficacy struct {
	DamageTypeID int `gorm:"column:damage_type_id;primary_key" json:"damage_type_id"`
	TargetTypeID int `gorm:"column:target_type_id" json:"target_type_id"`
	DamageFactor int `gorm:"column:damage_factor" json:"damage_factor"`
}

// TableName sets the insert table name for this struct type
func (t *TypeEfficacy) TableName() string {
	return "type_efficacy"
}
