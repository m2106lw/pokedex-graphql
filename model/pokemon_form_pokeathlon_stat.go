package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonFormPokeathlonStat struct {
	PokemonFormID    int `gorm:"column:pokemon_form_id;primary_key" json:"pokemon_form_id"`
	PokeathlonStatID int `gorm:"column:pokeathlon_stat_id" json:"pokeathlon_stat_id"`
	MinimumStat      int `gorm:"column:minimum_stat" json:"minimum_stat"`
	BaseStat         int `gorm:"column:base_stat" json:"base_stat"`
	MaximumStat      int `gorm:"column:maximum_stat" json:"maximum_stat"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonFormPokeathlonStat) TableName() string {
	return "pokemon_form_pokeathlon_stats"
}
