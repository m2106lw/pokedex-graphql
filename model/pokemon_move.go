package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonMove struct {
	PokemonID           int `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	VersionGroupID      int `gorm:"column:version_group_id" json:"version_group_id"`
	MoveID              int `gorm:"column:move_id" json:"move_id"`
	PokemonMoveMethodID int `gorm:"column:pokemon_move_method_id" json:"pokemon_move_method_id"`
	Level               int `gorm:"column:level" json:"level"`
	Order               int `gorm:"column:order" json:"order"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonMove) TableName() string {
	return "pokemon_moves"
}
