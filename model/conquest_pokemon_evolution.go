package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestPokemonEvolution struct {
	EvolvedSpeciesID     int  `gorm:"column:evolved_species_id;primary_key" json:"evolved_species_id"`
	RequiredStatID       int  `gorm:"column:required_stat_id" json:"required_stat_id"`
	MinimumStat          int  `gorm:"column:minimum_stat" json:"minimum_stat"`
	MinimumLink          int  `gorm:"column:minimum_link" json:"minimum_link"`
	KingdomID            int  `gorm:"column:kingdom_id" json:"kingdom_id"`
	WarriorGenderID      int  `gorm:"column:warrior_gender_id" json:"warrior_gender_id"`
	ItemID               int  `gorm:"column:item_id" json:"item_id"`
	RecruitingKoRequired bool `gorm:"column:recruiting_ko_required" json:"recruiting_ko_required"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestPokemonEvolution) TableName() string {
	return "conquest_pokemon_evolution"
}
