package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type AbilityChangelogProse struct {
	AbilityChangelogID int    `gorm:"column:ability_changelog_id;primary_key" json:"ability_changelog_id"`
	LocalLanguageID    int    `gorm:"column:local_language_id" json:"local_language_id"`
	Effect             string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (a *AbilityChangelogProse) TableName() string {
	return "ability_changelog_prose"
}
