package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveDisplacementProse struct {
	MoveDisplacementID int    `gorm:"column:move_displacement_id;primary_key" json:"move_displacement_id"`
	LocalLanguageID    int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name               string `gorm:"column:name" json:"name"`
	ShortEffect        string `gorm:"column:short_effect" json:"short_effect"`
	Effect             string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveDisplacementProse) TableName() string {
	return "conquest_move_displacement_prose"
}
