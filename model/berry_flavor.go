package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type BerryFlavor struct {
	BerryID       int `gorm:"column:berry_id;primary_key" json:"berry_id"`
	ContestTypeID int `gorm:"column:contest_type_id" json:"contest_type_id"`
	Flavor        int `gorm:"column:flavor" json:"flavor"`
}

// TableName sets the insert table name for this struct type
func (b *BerryFlavor) TableName() string {
	return "berry_flavors"
}
