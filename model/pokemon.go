package model

type Pokemon struct {
	ID             int    `gorm:"column:id;primary_key" json:"id"`
	Identifier     string `gorm:"column:identifier;not null" json:"identifier"`
	SpeciesID      int    `gorm:"column:species_id;not null" json:"species_id"`
	Height         int    `gorm:"column:height;not null" json:"height"`
	Weight         int    `gorm:"column:weight;not null" json:"weight"`
	BaseExperience int    `gorm:"column:base_experience;not null" json:"base_experience"`
	Order          int    `gorm:"column:order;not null" json:"order"`
	IsDefault      bool   `gorm:"column:is_default;not null" json:"is_default"`
}

// TableName sets the insert table name for this struct type
func (p *Pokemon) TableName() string {
	return "pokemon"
}
