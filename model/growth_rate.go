package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type GrowthRate struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	Formula    string `gorm:"column:formula" json:"formula"`
}

// TableName sets the insert table name for this struct type
func (g *GrowthRate) TableName() string {
	return "growth_rates"
}
