package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestStat struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	IsBase     bool   `gorm:"column:is_base" json:"is_base"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestStat) TableName() string {
	return "conquest_stats"
}
