package model

type PokemonGameIndex struct {
	PokemonID int `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	VersionID int `gorm:"column:version_id;not null" json:"version_id"`
	GameIndex int `gorm:"column:game_index;not null" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonGameIndex) TableName() string {
	return "pokemon_game_indices"
}
