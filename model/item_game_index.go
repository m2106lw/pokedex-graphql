package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemGameIndex struct {
	ItemID       int `gorm:"column:item_id;primary_key" json:"item_id"`
	GenerationID int `gorm:"column:generation_id" json:"generation_id"`
	GameIndex    int `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (i *ItemGameIndex) TableName() string {
	return "item_game_indices"
}
