package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type LocationAreaProse struct {
	LocationAreaID  int    `gorm:"column:location_area_id;primary_key" json:"location_area_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (l *LocationAreaProse) TableName() string {
	return "location_area_prose"
}
