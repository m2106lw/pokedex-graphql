package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Machine struct {
	MachineNumber  int `gorm:"column:machine_number;primary_key" json:"machine_number"`
	VersionGroupID int `gorm:"column:version_group_id" json:"version_group_id"`
	ItemID         int `gorm:"column:item_id" json:"item_id"`
	MoveID         int `gorm:"column:move_id" json:"move_id"`
}

// TableName sets the insert table name for this struct type
func (m *Machine) TableName() string {
	return "machines"
}
