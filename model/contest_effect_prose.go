package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ContestEffectProse struct {
	ContestEffectID int    `gorm:"column:contest_effect_id;primary_key" json:"contest_effect_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	FlavorText      string `gorm:"column:flavor_text" json:"flavor_text"`
	Effect          string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (c *ContestEffectProse) TableName() string {
	return "contest_effect_prose"
}
