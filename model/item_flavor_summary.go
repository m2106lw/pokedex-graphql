package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemFlavorSummary struct {
	ItemID          int    `gorm:"column:item_id;primary_key" json:"item_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	FlavorSummary   string `gorm:"column:flavor_summary" json:"flavor_summary"`
}

// TableName sets the insert table name for this struct type
func (i *ItemFlavorSummary) TableName() string {
	return "item_flavor_summaries"
}
