package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestEpisodeWarrior struct {
	EpisodeID int `gorm:"column:episode_id;primary_key" json:"episode_id"`
	WarriorID int `gorm:"column:warrior_id" json:"warrior_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestEpisodeWarrior) TableName() string {
	return "conquest_episode_warriors"
}
