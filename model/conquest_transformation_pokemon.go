package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestTransformationPokemon struct {
	TransformationID int `gorm:"column:transformation_id;primary_key" json:"transformation_id"`
	PokemonSpeciesID int `gorm:"column:pokemon_species_id" json:"pokemon_species_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestTransformationPokemon) TableName() string {
	return "conquest_transformation_pokemon"
}
