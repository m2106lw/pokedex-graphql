package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorRankStatMap struct {
	WarriorRankID int `gorm:"column:warrior_rank_id;primary_key" json:"warrior_rank_id"`
	WarriorStatID int `gorm:"column:warrior_stat_id" json:"warrior_stat_id"`
	BaseStat      int `gorm:"column:base_stat" json:"base_stat"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorRankStatMap) TableName() string {
	return "conquest_warrior_rank_stat_map"
}
