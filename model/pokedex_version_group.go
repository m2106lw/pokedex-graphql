package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokedexVersionGroup struct {
	PokedexID      int `gorm:"column:pokedex_id;primary_key" json:"pokedex_id"`
	VersionGroupID int `gorm:"column:version_group_id" json:"version_group_id"`
}

// TableName sets the insert table name for this struct type
func (p *PokedexVersionGroup) TableName() string {
	return "pokedex_version_groups"
}
