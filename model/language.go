package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Language struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Iso639     string `gorm:"column:iso639" json:"iso639"`
	Iso3166    string `gorm:"column:iso3166" json:"iso3166"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	Official   bool   `gorm:"column:official" json:"official"`
	Order      int    `gorm:"column:order" json:"order"`
}

// TableName sets the insert table name for this struct type
func (l *Language) TableName() string {
	return "languages"
}
