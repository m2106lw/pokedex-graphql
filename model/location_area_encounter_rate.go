package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type LocationAreaEncounterRate struct {
	LocationAreaID    int `gorm:"column:location_area_id;primary_key" json:"location_area_id"`
	EncounterMethodID int `gorm:"column:encounter_method_id" json:"encounter_method_id"`
	VersionID         int `gorm:"column:version_id" json:"version_id"`
	Rate              int `gorm:"column:rate" json:"rate"`
}

// TableName sets the insert table name for this struct type
func (l *LocationAreaEncounterRate) TableName() string {
	return "location_area_encounter_rates"
}
