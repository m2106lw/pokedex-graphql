package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type LocationArea struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	LocationID int    `gorm:"column:location_id" json:"location_id"`
	GameIndex  int    `gorm:"column:game_index" json:"game_index"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (l *LocationArea) TableName() string {
	return "location_areas"
}
