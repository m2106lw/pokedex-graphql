package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemFlavorText struct {
	ItemID         int    `gorm:"column:item_id;primary_key" json:"item_id"`
	VersionGroupID int    `gorm:"column:version_group_id" json:"version_group_id"`
	LanguageID     int    `gorm:"column:language_id" json:"language_id"`
	FlavorText     string `gorm:"column:flavor_text" json:"flavor_text"`
}

// TableName sets the insert table name for this struct type
func (i *ItemFlavorText) TableName() string {
	return "item_flavor_text"
}
