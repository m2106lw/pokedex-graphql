package model

type PokemonType struct {
	PokemonID int `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	TypeID    int `gorm:"column:type_id;not null" json:"type_id"`
	Slot      int `gorm:"column:slot;not null" json:"slot"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonType) TableName() string {
	return "pokemon_types"
}
