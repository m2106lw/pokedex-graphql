package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestPokemonAbility struct {
	PokemonSpeciesID int `gorm:"column:pokemon_species_id;primary_key" json:"pokemon_species_id"`
	Slot             int `gorm:"column:slot" json:"slot"`
	AbilityID        int `gorm:"column:ability_id" json:"ability_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestPokemonAbility) TableName() string {
	return "conquest_pokemon_abilities"
}
