package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Characteristic struct {
	ID       int `gorm:"column:id;primary_key" json:"id"`
	StatID   int `gorm:"column:stat_id" json:"stat_id"`
	GeneMod5 int `gorm:"column:gene_mod_5" json:"gene_mod_5"`
}

// TableName sets the insert table name for this struct type
func (c *Characteristic) TableName() string {
	return "characteristics"
}
