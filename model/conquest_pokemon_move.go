package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestPokemonMove struct {
	PokemonSpeciesID int `gorm:"column:pokemon_species_id;primary_key" json:"pokemon_species_id"`
	MoveID           int `gorm:"column:move_id" json:"move_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestPokemonMove) TableName() string {
	return "conquest_pokemon_moves"
}
