package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestKingdom struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	TypeID     int    `gorm:"column:type_id" json:"type_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestKingdom) TableName() string {
	return "conquest_kingdoms"
}
