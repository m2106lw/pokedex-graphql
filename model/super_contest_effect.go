package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type SuperContestEffect struct {
	ID     int `gorm:"column:id;primary_key" json:"id"`
	Appeal int `gorm:"column:appeal" json:"appeal"`
}

// TableName sets the insert table name for this struct type
func (s *SuperContestEffect) TableName() string {
	return "super_contest_effects"
}
