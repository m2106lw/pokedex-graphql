package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterConditionValue struct {
	ID                   int    `gorm:"column:id;primary_key" json:"id"`
	EncounterConditionID int    `gorm:"column:encounter_condition_id" json:"encounter_condition_id"`
	Identifier           string `gorm:"column:identifier" json:"identifier"`
	IsDefault            bool   `gorm:"column:is_default" json:"is_default"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterConditionValue) TableName() string {
	return "encounter_condition_values"
}
