package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonColor struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonColor) TableName() string {
	return "pokemon_colors"
}
