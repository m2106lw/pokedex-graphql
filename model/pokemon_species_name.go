package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonSpeciesName struct {
	PokemonSpeciesID int    `gorm:"column:pokemon_species_id;primary_key" json:"pokemon_species_id"`
	LocalLanguageID  int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name             string `gorm:"column:name" json:"name"`
	Genus            string `gorm:"column:genus" json:"genus"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonSpeciesName) TableName() string {
	return "pokemon_species_names"
}
