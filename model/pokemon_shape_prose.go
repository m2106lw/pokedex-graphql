package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonShapeProse struct {
	PokemonShapeID  int    `gorm:"column:pokemon_shape_id;primary_key" json:"pokemon_shape_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
	AwesomeName     string `gorm:"column:awesome_name" json:"awesome_name"`
	Description     string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonShapeProse) TableName() string {
	return "pokemon_shape_prose"
}
