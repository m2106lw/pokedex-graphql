package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterMethodProse struct {
	EncounterMethodID int    `gorm:"column:encounter_method_id;primary_key" json:"encounter_method_id"`
	LocalLanguageID   int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name              string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterMethodProse) TableName() string {
	return "encounter_method_prose"
}
