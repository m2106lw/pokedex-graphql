package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type VersionGroup struct {
	ID           int    `gorm:"column:id;primary_key" json:"id"`
	Identifier   string `gorm:"column:identifier" json:"identifier"`
	GenerationID int    `gorm:"column:generation_id" json:"generation_id"`
	Order        int    `gorm:"column:order" json:"order"`
}

// TableName sets the insert table name for this struct type
func (v *VersionGroup) TableName() string {
	return "version_groups"
}
