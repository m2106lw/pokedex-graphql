package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveRange struct {
	ID         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier string `gorm:"column:identifier" json:"identifier"`
	Targets    int    `gorm:"column:targets" json:"targets"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveRange) TableName() string {
	return "conquest_move_ranges"
}
