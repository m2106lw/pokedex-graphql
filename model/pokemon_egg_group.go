package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonEggGroup struct {
	SpeciesID  int `gorm:"column:species_id;primary_key" json:"species_id"`
	EggGroupID int `gorm:"column:egg_group_id" json:"egg_group_id"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonEggGroup) TableName() string {
	return "pokemon_egg_groups"
}
