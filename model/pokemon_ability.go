package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonAbility struct {
	PokemonID int  `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	AbilityID int  `gorm:"column:ability_id" json:"ability_id"`
	IsHidden  bool `gorm:"column:is_hidden" json:"is_hidden"`
	Slot      int  `gorm:"column:slot" json:"slot"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonAbility) TableName() string {
	return "pokemon_abilities"
}
