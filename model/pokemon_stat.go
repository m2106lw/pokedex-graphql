package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonStat struct {
	PokemonID int `gorm:"column:pokemon_id;primary_key" json:"pokemon_id"`
	StatID    int `gorm:"column:stat_id" json:"stat_id"`
	BaseStat  int `gorm:"column:base_stat" json:"base_stat"`
	Effort    int `gorm:"column:effort" json:"effort"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonStat) TableName() string {
	return "pokemon_stats"
}
