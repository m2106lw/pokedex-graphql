package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type SuperContestCombo struct {
	FirstMoveID  int `gorm:"column:first_move_id;primary_key" json:"first_move_id"`
	SecondMoveID int `gorm:"column:second_move_id" json:"second_move_id"`
}

// TableName sets the insert table name for this struct type
func (s *SuperContestCombo) TableName() string {
	return "super_contest_combos"
}
