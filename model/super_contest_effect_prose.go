package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type SuperContestEffectProse struct {
	SuperContestEffectID int    `gorm:"column:super_contest_effect_id;primary_key" json:"super_contest_effect_id"`
	LocalLanguageID      int    `gorm:"column:local_language_id" json:"local_language_id"`
	FlavorText           string `gorm:"column:flavor_text" json:"flavor_text"`
}

// TableName sets the insert table name for this struct type
func (s *SuperContestEffectProse) TableName() string {
	return "super_contest_effect_prose"
}
