package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonSpeciesFlavorText struct {
	SpeciesID  int    `gorm:"column:species_id;primary_key" json:"species_id"`
	VersionID  int    `gorm:"column:version_id" json:"version_id"`
	LanguageID int    `gorm:"column:language_id" json:"language_id"`
	FlavorText string `gorm:"column:flavor_text" json:"flavor_text"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonSpeciesFlavorText) TableName() string {
	return "pokemon_species_flavor_text"
}
