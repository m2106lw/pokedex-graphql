package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EggGroupProse struct {
	EggGroupID      int    `gorm:"column:egg_group_id;primary_key" json:"egg_group_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (e *EggGroupProse) TableName() string {
	return "egg_group_prose"
}
