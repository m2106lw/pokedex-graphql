package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveFlagProse struct {
	MoveFlagID      int    `gorm:"column:move_flag_id;primary_key" json:"move_flag_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
	Description     string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (m *MoveFlagProse) TableName() string {
	return "move_flag_prose"
}
