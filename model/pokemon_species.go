package model

type PokemonSpecies struct {
	ID                   int    `gorm:"column:id;primary_key" json:"id"`
	Identifier           string `gorm:"column:identifier;not null" json:"identifier"`
	GenerationID         int    `gorm:"column:generation_id" json:"generation_id"`
	EvolvesFromSpeciesID int    `gorm:"column:evolves_from_species_id" json:"evolves_from_species_id"`
	EvolutionChainID     int    `gorm:"column:evolution_chain_id" json:"evolution_chain_id"`
	ColorID              int    `gorm:"column:color_id;not null" json:"color_id"`
	ShapeID              int    `gorm:"column:shape_id;not null" json:"shape_id"`
	HabitatID            int    `gorm:"column:habitat_id" json:"habitat_id"`
	GenderRate           int    `gorm:"column:gender_rate;not null" json:"gender_rate"`
	CaptureRate          int    `gorm:"column:capture_rate;not null" json:"capture_rate"`
	BaseHappiness        int    `gorm:"column:base_happiness;not null" json:"base_happiness"`
	IsBaby               bool   `gorm:"column:is_baby;not null" json:"is_baby"`
	HatchCounter         int    `gorm:"column:hatch_counter;not null" json:"hatch_counter"`
	HasGenderDifferences bool   `gorm:"column:has_gender_differences;not null" json:"has_gender_differences"`
	GrowthRateID         int    `gorm:"column:growth_rate_id;not null" json:"growth_rate_id"`
	FormsSwitchable      bool   `gorm:"column:forms_switchable;not null" json:"forms_switchable"`
	Order                int    `gorm:"column:order;not null" json:"order"`
	ConquestOrder        int    `gorm:"column:conquest_order" json:"conquest_order"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonSpecies) TableName() string {
	return "pokemon_species"
}
