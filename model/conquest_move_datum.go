package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveDatum struct {
	MoveID         int `gorm:"column:move_id;primary_key" json:"move_id"`
	Power          int `gorm:"column:power" json:"power"`
	Accuracy       int `gorm:"column:accuracy" json:"accuracy"`
	EffectChance   int `gorm:"column:effect_chance" json:"effect_chance"`
	EffectID       int `gorm:"column:effect_id" json:"effect_id"`
	RangeID        int `gorm:"column:range_id" json:"range_id"`
	DisplacementID int `gorm:"column:displacement_id" json:"displacement_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveDatum) TableName() string {
	return "conquest_move_data"
}
