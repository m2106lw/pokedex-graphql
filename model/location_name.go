package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type LocationName struct {
	LocationID      int    `gorm:"column:location_id;primary_key" json:"location_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
	Subtitle        string `gorm:"column:subtitle" json:"subtitle"`
}

// TableName sets the insert table name for this struct type
func (l *LocationName) TableName() string {
	return "location_names"
}
