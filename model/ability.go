package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Ability struct {
	ID           int    `gorm:"column:id;primary_key" json:"id"`
	Identifier   string `gorm:"column:identifier" json:"identifier"`
	GenerationID int    `gorm:"column:generation_id" json:"generation_id"`
	IsMainSeries bool   `gorm:"column:is_main_series" json:"is_main_series"`
}

// TableName sets the insert table name for this struct type
func (a *Ability) TableName() string {
	return "abilities"
}
