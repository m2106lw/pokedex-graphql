package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ContestEffect struct {
	ID     int `gorm:"column:id;primary_key" json:"id"`
	Appeal int `gorm:"column:appeal" json:"appeal"`
	Jam    int `gorm:"column:jam" json:"jam"`
}

// TableName sets the insert table name for this struct type
func (c *ContestEffect) TableName() string {
	return "contest_effects"
}
