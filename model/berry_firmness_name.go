package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type BerryFirmnessName struct {
	BerryFirmnessID int    `gorm:"column:berry_firmness_id;primary_key" json:"berry_firmness_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (b *BerryFirmnessName) TableName() string {
	return "berry_firmness_names"
}
