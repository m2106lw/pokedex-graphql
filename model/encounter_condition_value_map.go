package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterConditionValueMap struct {
	EncounterID               int `gorm:"column:encounter_id;primary_key" json:"encounter_id"`
	EncounterConditionValueID int `gorm:"column:encounter_condition_value_id" json:"encounter_condition_value_id"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterConditionValueMap) TableName() string {
	return "encounter_condition_value_map"
}
