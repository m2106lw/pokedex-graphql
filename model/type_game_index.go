package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type TypeGameIndex struct {
	TypeID       int `gorm:"column:type_id;primary_key" json:"type_id"`
	GenerationID int `gorm:"column:generation_id" json:"generation_id"`
	GameIndex    int `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (t *TypeGameIndex) TableName() string {
	return "type_game_indices"
}
