package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveTargetProse struct {
	MoveTargetID    int    `gorm:"column:move_target_id;primary_key" json:"move_target_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
	Description     string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (m *MoveTargetProse) TableName() string {
	return "move_target_prose"
}
