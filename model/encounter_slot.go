package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterSlot struct {
	ID                int `gorm:"column:id;primary_key" json:"id"`
	VersionGroupID    int `gorm:"column:version_group_id" json:"version_group_id"`
	EncounterMethodID int `gorm:"column:encounter_method_id" json:"encounter_method_id"`
	Slot              int `gorm:"column:slot" json:"slot"`
	Rarity            int `gorm:"column:rarity" json:"rarity"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterSlot) TableName() string {
	return "encounter_slots"
}
