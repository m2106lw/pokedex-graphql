package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonForm struct {
	ID                         int    `gorm:"column:id;primary_key" json:"id"`
	Identifier                 string `gorm:"column:identifier" json:"identifier"`
	FormIdentifier             string `gorm:"column:form_identifier" json:"form_identifier"`
	PokemonID                  int    `gorm:"column:pokemon_id" json:"pokemon_id"`
	IntroducedInVersionGroupID int    `gorm:"column:introduced_in_version_group_id" json:"introduced_in_version_group_id"`
	IsDefault                  bool   `gorm:"column:is_default" json:"is_default"`
	IsBattleOnly               bool   `gorm:"column:is_battle_only" json:"is_battle_only"`
	IsMega                     bool   `gorm:"column:is_mega" json:"is_mega"`
	FormOrder                  int    `gorm:"column:form_order" json:"form_order"`
	Order                      int    `gorm:"column:order" json:"order"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonForm) TableName() string {
	return "pokemon_forms"
}
