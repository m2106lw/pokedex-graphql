package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonHabitatName struct {
	PokemonHabitatID int    `gorm:"column:pokemon_habitat_id;primary_key" json:"pokemon_habitat_id"`
	LocalLanguageID  int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name             string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonHabitatName) TableName() string {
	return "pokemon_habitat_names"
}
