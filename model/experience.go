package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Experience struct {
	GrowthRateID int `gorm:"column:growth_rate_id;primary_key" json:"growth_rate_id"`
	Level        int `gorm:"column:level" json:"level"`
	Experience   int `gorm:"column:experience" json:"experience"`
}

// TableName sets the insert table name for this struct type
func (e *Experience) TableName() string {
	return "experience"
}
