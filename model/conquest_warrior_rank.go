package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorRank struct {
	ID        int `gorm:"column:id;primary_key" json:"id"`
	WarriorID int `gorm:"column:warrior_id" json:"warrior_id"`
	Rank      int `gorm:"column:rank" json:"rank"`
	SkillID   int `gorm:"column:skill_id" json:"skill_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorRank) TableName() string {
	return "conquest_warrior_ranks"
}
