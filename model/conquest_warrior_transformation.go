package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorTransformation struct {
	TransformedWarriorRankID int  `gorm:"column:transformed_warrior_rank_id;primary_key" json:"transformed_warrior_rank_id"`
	IsAutomatic              bool `gorm:"column:is_automatic" json:"is_automatic"`
	RequiredLink             int  `gorm:"column:required_link" json:"required_link"`
	CompletedEpisodeID       int  `gorm:"column:completed_episode_id" json:"completed_episode_id"`
	CurrentEpisodeID         int  `gorm:"column:current_episode_id" json:"current_episode_id"`
	DistantWarriorID         int  `gorm:"column:distant_warrior_id" json:"distant_warrior_id"`
	FemaleWarlordCount       int  `gorm:"column:female_warlord_count" json:"female_warlord_count"`
	PokemonCount             int  `gorm:"column:pokemon_count" json:"pokemon_count"`
	CollectionTypeID         int  `gorm:"column:collection_type_id" json:"collection_type_id"`
	WarriorCount             int  `gorm:"column:warrior_count" json:"warrior_count"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorTransformation) TableName() string {
	return "conquest_warrior_transformation"
}
