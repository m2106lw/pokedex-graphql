package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type VersionGroupPokemonMoveMethod struct {
	VersionGroupID      int `gorm:"column:version_group_id;primary_key" json:"version_group_id"`
	PokemonMoveMethodID int `gorm:"column:pokemon_move_method_id" json:"pokemon_move_method_id"`
}

// TableName sets the insert table name for this struct type
func (v *VersionGroupPokemonMoveMethod) TableName() string {
	return "version_group_pokemon_move_methods"
}
