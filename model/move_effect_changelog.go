package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveEffectChangelog struct {
	ID                      int `gorm:"column:id;primary_key" json:"id"`
	EffectID                int `gorm:"column:effect_id" json:"effect_id"`
	ChangedInVersionGroupID int `gorm:"column:changed_in_version_group_id" json:"changed_in_version_group_id"`
}

// TableName sets the insert table name for this struct type
func (m *MoveEffectChangelog) TableName() string {
	return "move_effect_changelog"
}
