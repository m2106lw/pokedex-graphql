package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveEffectChangelogProse struct {
	MoveEffectChangelogID int    `gorm:"column:move_effect_changelog_id;primary_key" json:"move_effect_changelog_id"`
	LocalLanguageID       int    `gorm:"column:local_language_id" json:"local_language_id"`
	Effect                string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (m *MoveEffectChangelogProse) TableName() string {
	return "move_effect_changelog_prose"
}
