package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PalPark struct {
	SpeciesID int `gorm:"column:species_id;primary_key" json:"species_id"`
	AreaID    int `gorm:"column:area_id" json:"area_id"`
	BaseScore int `gorm:"column:base_score" json:"base_score"`
	Rate      int `gorm:"column:rate" json:"rate"`
}

// TableName sets the insert table name for this struct type
func (p *PalPark) TableName() string {
	return "pal_park"
}
