package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonFormName struct {
	PokemonFormID   int    `gorm:"column:pokemon_form_id;primary_key" json:"pokemon_form_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	FormName        string `gorm:"column:form_name" json:"form_name"`
	PokemonName     string `gorm:"column:pokemon_name" json:"pokemon_name"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonFormName) TableName() string {
	return "pokemon_form_names"
}
