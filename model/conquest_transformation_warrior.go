package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestTransformationWarrior struct {
	TransformationID int `gorm:"column:transformation_id;primary_key" json:"transformation_id"`
	PresentWarriorID int `gorm:"column:present_warrior_id" json:"present_warrior_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestTransformationWarrior) TableName() string {
	return "conquest_transformation_warriors"
}
