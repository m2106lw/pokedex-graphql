package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EvolutionChain struct {
	ID                int `gorm:"column:id;primary_key" json:"id"`
	BabyTriggerItemID int `gorm:"column:baby_trigger_item_id" json:"baby_trigger_item_id"`
}

// TableName sets the insert table name for this struct type
func (e *EvolutionChain) TableName() string {
	return "evolution_chains"
}
