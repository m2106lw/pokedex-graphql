package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type GrowthRateProse struct {
	GrowthRateID    int    `gorm:"column:growth_rate_id;primary_key" json:"growth_rate_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (g *GrowthRateProse) TableName() string {
	return "growth_rate_prose"
}
