package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonFormGeneration struct {
	PokemonFormID int `gorm:"column:pokemon_form_id;primary_key" json:"pokemon_form_id"`
	GenerationID  int `gorm:"column:generation_id" json:"generation_id"`
	GameIndex     int `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonFormGeneration) TableName() string {
	return "pokemon_form_generations"
}
