package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokeathlonStatName struct {
	PokeathlonStatID int    `gorm:"column:pokeathlon_stat_id;primary_key" json:"pokeathlon_stat_id"`
	LocalLanguageID  int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name             string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (p *PokeathlonStatName) TableName() string {
	return "pokeathlon_stat_names"
}
