package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveMetaCategoryProse struct {
	MoveMetaCategoryID int    `gorm:"column:move_meta_category_id;primary_key" json:"move_meta_category_id"`
	LocalLanguageID    int    `gorm:"column:local_language_id" json:"local_language_id"`
	Description        string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (m *MoveMetaCategoryProse) TableName() string {
	return "move_meta_category_prose"
}
