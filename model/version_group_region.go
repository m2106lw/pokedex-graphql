package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type VersionGroupRegion struct {
	VersionGroupID int `gorm:"column:version_group_id;primary_key" json:"version_group_id"`
	RegionID       int `gorm:"column:region_id" json:"region_id"`
}

// TableName sets the insert table name for this struct type
func (v *VersionGroupRegion) TableName() string {
	return "version_group_regions"
}
