package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorName struct {
	WarriorID       int    `gorm:"column:warrior_id;primary_key" json:"warrior_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorName) TableName() string {
	return "conquest_warrior_names"
}
