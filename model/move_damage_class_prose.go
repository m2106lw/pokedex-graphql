package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveDamageClassProse struct {
	MoveDamageClassID int    `gorm:"column:move_damage_class_id;primary_key" json:"move_damage_class_id"`
	LocalLanguageID   int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name              string `gorm:"column:name" json:"name"`
	Description       string `gorm:"column:description" json:"description"`
}

// TableName sets the insert table name for this struct type
func (m *MoveDamageClassProse) TableName() string {
	return "move_damage_class_prose"
}
