package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemCategoryProse struct {
	ItemCategoryID  int    `gorm:"column:item_category_id;primary_key" json:"item_category_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (i *ItemCategoryProse) TableName() string {
	return "item_category_prose"
}
