package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type EncounterConditionProse struct {
	EncounterConditionID int    `gorm:"column:encounter_condition_id;primary_key" json:"encounter_condition_id"`
	LocalLanguageID      int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name                 string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (e *EncounterConditionProse) TableName() string {
	return "encounter_condition_prose"
}
