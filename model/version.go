package model

type Version struct {
	ID             int    `gorm:"column:id;primary_key" json:"id"`
	VersionGroupID int    `gorm:"column:version_group_id;not null" json:"version_group_id"`
	Identifier     string `gorm:"column:identifier;not null" json:"identifier"`
}

// TableName sets the insert table name for this struct type
func (v *Version) TableName() string {
	return "versions"
}
