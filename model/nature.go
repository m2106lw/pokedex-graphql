package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Nature struct {
	ID              int    `gorm:"column:id;primary_key" json:"id"`
	Identifier      string `gorm:"column:identifier" json:"identifier"`
	DecreasedStatID int    `gorm:"column:decreased_stat_id" json:"decreased_stat_id"`
	IncreasedStatID int    `gorm:"column:increased_stat_id" json:"increased_stat_id"`
	HatesFlavorID   int    `gorm:"column:hates_flavor_id" json:"hates_flavor_id"`
	LikesFlavorID   int    `gorm:"column:likes_flavor_id" json:"likes_flavor_id"`
	GameIndex       int    `gorm:"column:game_index" json:"game_index"`
}

// TableName sets the insert table name for this struct type
func (n *Nature) TableName() string {
	return "natures"
}
