package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ItemFlingEffectProse struct {
	ItemFlingEffectID int    `gorm:"column:item_fling_effect_id;primary_key" json:"item_fling_effect_id"`
	LocalLanguageID   int    `gorm:"column:local_language_id" json:"local_language_id"`
	Effect            string `gorm:"column:effect" json:"effect"`
}

// TableName sets the insert table name for this struct type
func (i *ItemFlingEffectProse) TableName() string {
	return "item_fling_effect_prose"
}
