package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMaxLink struct {
	WarriorRankID    int `gorm:"column:warrior_rank_id;primary_key" json:"warrior_rank_id"`
	PokemonSpeciesID int `gorm:"column:pokemon_species_id" json:"pokemon_species_id"`
	MaxLink          int `gorm:"column:max_link" json:"max_link"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMaxLink) TableName() string {
	return "conquest_max_links"
}
