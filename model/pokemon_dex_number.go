package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonDexNumber struct {
	SpeciesID     int `gorm:"column:species_id;primary_key" json:"species_id"`
	PokedexID     int `gorm:"column:pokedex_id" json:"pokedex_id"`
	PokedexNumber int `gorm:"column:pokedex_number" json:"pokedex_number"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonDexNumber) TableName() string {
	return "pokemon_dex_numbers"
}
