package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type RegionName struct {
	RegionID        int    `gorm:"column:region_id;primary_key" json:"region_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (r *RegionName) TableName() string {
	return "region_names"
}
