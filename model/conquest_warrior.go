package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarrior struct {
	ID          int    `gorm:"column:id;primary_key" json:"id"`
	Identifier  string `gorm:"column:identifier" json:"identifier"`
	GenderID    int    `gorm:"column:gender_id" json:"gender_id"`
	ArchetypeID int    `gorm:"column:archetype_id" json:"archetype_id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarrior) TableName() string {
	return "conquest_warriors"
}
