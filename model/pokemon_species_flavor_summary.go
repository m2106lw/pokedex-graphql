package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonSpeciesFlavorSummary struct {
	PokemonSpeciesID int    `gorm:"column:pokemon_species_id;primary_key" json:"pokemon_species_id"`
	LocalLanguageID  int    `gorm:"column:local_language_id" json:"local_language_id"`
	FlavorSummary    string `gorm:"column:flavor_summary" json:"flavor_summary"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonSpeciesFlavorSummary) TableName() string {
	return "pokemon_species_flavor_summaries"
}
