package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Item struct {
	ID            int    `gorm:"column:id;primary_key" json:"id"`
	Identifier    string `gorm:"column:identifier" json:"identifier"`
	CategoryID    int    `gorm:"column:category_id" json:"category_id"`
	Cost          int    `gorm:"column:cost" json:"cost"`
	FlingPower    int    `gorm:"column:fling_power" json:"fling_power"`
	FlingEffectID int    `gorm:"column:fling_effect_id" json:"fling_effect_id"`
}

// TableName sets the insert table name for this struct type
func (i *Item) TableName() string {
	return "items"
}
