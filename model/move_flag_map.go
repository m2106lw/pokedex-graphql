package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type MoveFlagMap struct {
	MoveID     int `gorm:"column:move_id;primary_key" json:"move_id"`
	MoveFlagID int `gorm:"column:move_flag_id" json:"move_flag_id"`
}

// TableName sets the insert table name for this struct type
func (m *MoveFlagMap) TableName() string {
	return "move_flag_map"
}
