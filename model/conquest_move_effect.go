package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestMoveEffect struct {
	ID int `gorm:"column:id;primary_key" json:"id"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestMoveEffect) TableName() string {
	return "conquest_move_effects"
}
