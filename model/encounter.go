package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type Encounter struct {
	ID              int `gorm:"column:id;primary_key" json:"id"`
	VersionID       int `gorm:"column:version_id" json:"version_id"`
	LocationAreaID  int `gorm:"column:location_area_id" json:"location_area_id"`
	EncounterSlotID int `gorm:"column:encounter_slot_id" json:"encounter_slot_id"`
	PokemonID       int `gorm:"column:pokemon_id" json:"pokemon_id"`
	MinLevel        int `gorm:"column:min_level" json:"min_level"`
	MaxLevel        int `gorm:"column:max_level" json:"max_level"`
}

// TableName sets the insert table name for this struct type
func (e *Encounter) TableName() string {
	return "encounters"
}
