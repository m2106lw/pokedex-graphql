package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type PokemonColorName struct {
	PokemonColorID  int    `gorm:"column:pokemon_color_id;primary_key" json:"pokemon_color_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (p *PokemonColorName) TableName() string {
	return "pokemon_color_names"
}
