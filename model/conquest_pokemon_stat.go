package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestPokemonStat struct {
	PokemonSpeciesID int `gorm:"column:pokemon_species_id;primary_key" json:"pokemon_species_id"`
	ConquestStatID   int `gorm:"column:conquest_stat_id" json:"conquest_stat_id"`
	BaseStat         int `gorm:"column:base_stat" json:"base_stat"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestPokemonStat) TableName() string {
	return "conquest_pokemon_stats"
}
