package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type ConquestWarriorStatName struct {
	WarriorStatID   int    `gorm:"column:warrior_stat_id;primary_key" json:"warrior_stat_id"`
	LocalLanguageID int    `gorm:"column:local_language_id" json:"local_language_id"`
	Name            string `gorm:"column:name" json:"name"`
}

// TableName sets the insert table name for this struct type
func (c *ConquestWarriorStatName) TableName() string {
	return "conquest_warrior_stat_names"
}
