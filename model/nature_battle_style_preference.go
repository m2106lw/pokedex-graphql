package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
)

type NatureBattleStylePreference struct {
	NatureID          int `gorm:"column:nature_id;primary_key" json:"nature_id"`
	MoveBattleStyleID int `gorm:"column:move_battle_style_id" json:"move_battle_style_id"`
	LowHpPreference   int `gorm:"column:low_hp_preference" json:"low_hp_preference"`
	HighHpPreference  int `gorm:"column:high_hp_preference" json:"high_hp_preference"`
}

// TableName sets the insert table name for this struct type
func (n *NatureBattleStylePreference) TableName() string {
	return "nature_battle_style_preferences"
}
