# Pokedex Grapqhl

---

I'm doing this mostly for learning, both graphql and Go. This is very much a work in progress. Hell I might be doing it all wrong, who knows.

I've organized the existing tables into groups, though they are of course interconnected. 
I figure these will be the "hubs" where you can make individual queries against like pokemon or type. The entrypoints will be show in **bold**.
They can still show up in other queries, but you can collected the info separately if wanted, though I suppose graphql allows you to do that.
These are also for me to keep track of my progress.

---

### Ability

### Berry

### Conquest

### Contest

### Egg Group

### Encounter

### Evolution

### Experience
- experience

### Generation

### Growth Rate

### Item

### Language

### Location

### Machine
- machine

### Move
- move_battle_style
- move_battle_style_prose
- move_changelog
- move_damage_class
- move_damage_class_prose
- move_effect_changelog
- move_effect_changelog_prose
- move_effect
- move_effect_prose
- move_flag
- move_flag_map
- move_flag_prose
- move_flavor_summary
- move_flavor_text
- **move**
- move_meta_ailment
- move_meta_ailment_name
- move_meta_catry
- move_meta_catry_prose
- move_meta_stat_change
- move_metum
- move_name
- move_target
- move_target_prose

### Nature

### Pal Park

### Pokeathlon

### Pokedex
- **pokedex**
- pokedex_prose
- pokedex_version_group

### Pokemon Path
- pokemon_ability
- pokemon_color
- pokemon_color_name
- pokemon_dex_number
- pokemon_egg_group
- pokemon_evolution
- pokemon_form_generation
- pokemon_form
- pokemon_form_name
- pokemon_form_pokeathlon_stat
- pokemon_game_index: Done
- **pokemon**: Still working on. It is one of the "hubs" of the API that will be worked on for awhile now
- pokemon_habitat
- pokemon_habitat_name
- pokemon_item
- pokemon_move
- pokemon_move_method
- pokemon_move_method_prose
- pokemon_shape
- pokemon_shape_prose
- pokemon_species_flavor_summary
- pokemon_species_flavor_text
- pokemon_species: Need to connect any other tables that match up besides Pokemon
- pokemon_species_name
- pokemon_species_prose
- pokemon_stat
- pokemon_type: Done

### Region
- region_name
- **region**

### Stat
- stat_name
- **stat**

### Super Contest
- super_contest_combo
- super_contest_effect
- super_contest_effect_prose

### Type Path
- type_efficacy
- type_game_index
- **type**: Started, need to connect pieces
- type_name

### Version Path
- **version**: Need to connect all the pieces
- version_group: Started
- version_group_pokemon_move_method
- version_group_region
- version_name