package main

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/friendsofgo/graphiql"
	"github.com/gorilla/mux"
	"github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/resolver"
	"gitlab.com/m2106lw/pokedex-graphql/schema"
	//"github.com/juju/errors"
	//"go.uber.org/zap"
)

func main() {
	loadConfiguration()

	/// Start up the db connection
	dbChan := make(chan *database.DBInfo)
	go func() {
		database.StartDBConnection()
		dbChan <- database.DB
	}()

	// Setup the port
	port := string(':') + os.Getenv("PORT")
	if port == ":" {
		port = ":8080"
	}

	// Now begin loading routes
	//router := mux.NewRouter().StrictSlash(true)
	//routes.RouteHandler(router)

	dbConn := <-dbChan
	root, err := resolver.NewRoot(dbConn)
	if err != nil {
		log.Fatal(err)
	}

	opts := []graphql.SchemaOpt{graphql.UseFieldResolvers()}
	queryHandler := graphql.MustParseSchema(schema.String(), root, opts...)
	graphiqlHandler, err := graphiql.NewGraphiqlHandler("/graphql")
	if err != nil {
		panic(err)
	}

	router := mux.NewRouter().StrictSlash(false)
	handler := http.Handler(graphiqlHandler)
	//router.Methods("GET").Path("/").Handler(handler) //.HandlerFunc
	router.Path("/graphql").Handler(&relay.Handler{Schema: queryHandler})
	router.Path("/graphiql").Handler(handler)

	// Register handlers to routes.
	//mux := http.NewServeMux()
	//mux.Handle("/", handler.GraphiQL{})
	//mux.Handle("/graphql/", h)
	//mux.Handle("/graphql", h) // Register without a trailing slash to avoid redirect.

	// Check on cert status
	useTLS := checkCerts()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// This is declared now so that we can shut it down below
	var srv *http.Server

	// If the certs exist then we can use https
	// Else just use http
	if useTLS {
		// Most of the below came recommended, but I'm not sure what is required
		/*
			router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
				w.Write([]byte("This is an example server.\n"))
			})
		*/

		cfg := &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
		}
		srv = &http.Server{
			Addr:    port,
			Handler: router,
			//Handler:      mux,
			TLSConfig:    cfg,
			TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		}

		go func() {
			if err := srv.ListenAndServeTLS("certs/tls.crt", "certs/tls.key"); err != nil && err != http.ErrServerClosed {
				log.Fatalf("listen: %s\n", err)
			}
		}()
	} else {
		// Start the server
		srv = &http.Server{
			Handler: router,
			//Handler: mux,
			Addr: port,
			// Good practice: enforce timeouts for servers you create!
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		}

		go func() {
			if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				log.Fatalf("listen: %s\n", err)
			}
		}()
	}

	log.Println("Server Started")
	// Make sure we have a database connection and close it on exit - TODO: Improve this
	//dbConn := <-dbChan
	defer dbConn.Conn.Close()
	<-done
	log.Println("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		// extra handling here
		// Handle db connection?
		cancel()
	}()

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown Failed:%+v", err)
	}
	log.Println("Server Exited Properly")
}
