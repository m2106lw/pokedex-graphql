package schema

import (
	"io/ioutil"
	"log"
	"strings"
	"sync"
)

func readFile(wg *sync.WaitGroup, index int, graphqlString []string, filePath string) {
	defer wg.Done()

	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Println(err)
		return
	}

	graphqlString[index] = string(content)
	//log.Println(graphqlString)
}

func String() string {
	//files := []string{"./schema/schema.graphql", "./schema/query.graphql"}

	schemasDir := "./schema/schemas"
	files, err := ioutil.ReadDir(schemasDir)
	if err != nil {
		log.Fatal(err)
	}

	schemaFiles := []string{}
	schemaFiles = append(schemaFiles, "./schema/schema.graphql")
	schemaFiles = append(schemaFiles, "./schema/query.graphql")
	for _, file := range files {
		schemaFiles = append(schemaFiles, schemasDir+"/"+file.Name())
	}

	var wg sync.WaitGroup
	graphqlString := make([]string, len(schemaFiles))
	for i, file := range schemaFiles {
		wg.Add(1)
		go readFile(&wg, i, graphqlString, file)
	}

	wg.Wait()
	finalString := strings.Join(graphqlString, "\n")
	return finalString
}
