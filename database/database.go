package database

import (
	"log"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
	// TODO: Look into supporting multiple databases
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// DBInfo contains information about the database
// Add other info as needed
type DBInfo struct {
	Conn    *gorm.DB
	Dialect string
}

func (db DBInfo) IsRecordNotFound(err error) bool {
	return gorm.IsRecordNotFoundError(err)
}

// DB holds the database connection and the dialect, which may or may not be needed
var DB *DBInfo

// StartDBConnection will create a database connection to be used by the API
func StartDBConnection() {
	// TODO: Add port as a possible value
	databaseServer := os.Getenv("DATABASE_SERVER")
	databaseUser := os.Getenv("DATABASE_USER")
	databasePassword := os.Getenv("DATABASE_PASSWORD")
	databaseDatabase := os.Getenv("DATABASE_DATABASE")
	databasePort := os.Getenv("DATABASE_PORT")
	databaseDialect := os.Getenv("DATABASE_DIALECT")

	// Figure out a default port for a few db types
	if databasePort == "" && databaseDialect != "" {
		switch databaseDialect {
		case "postgres":
			databasePort = "5432"
		case "mysql":
			databasePort = "3306"
		case "mariadb":
			databasePort = "3306"
		case "sqlserver":
			databasePort = "1433"
		default:
			log.Fatal("Default port for " + databaseDialect + " cannot be determined")
		}
	}

	connectionString := "host=" + databaseServer + " port=" + databasePort + " user=" + databaseUser + " dbname=" + databaseDatabase + " password=" + databasePassword
	// TODO: Figure out how to best pass this in
	connectionString = connectionString + " sslmode=disable"

	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		log.Fatal("Connection to database failed:", err)
	}

	// Have gorm print out the queries it creates - for debugging
	databaseDebug, err := strconv.ParseBool(os.Getenv("DATABASE_DEBUG"))
	if err != nil {
		databaseDebug = false
		log.Println(err)
	}
	db.LogMode(databaseDebug)

	// Migrate the schema
	//db.AutoMigrate(&User{})
	//db.AutoMigrate(&Group{})
	//db.AutoMigrate(&Role{})
	//db.AutoMigrate(&Project{})

	// Save the database object and the dialect incase we need to modify some queries
	DB = &DBInfo{
		Conn:    db,
		Dialect: databaseDialect,
	}
}

/*
// TODO: Figure out if I will use this
// The idea here is the keep track of db connections and wait for them to finish on shutdown
var dbConnections sync.WaitGroup

// This will return the waitGroups used by the database
func GetDBWaitGroup() *sync.WaitGroup {
	return &dbConnections
}
*/
