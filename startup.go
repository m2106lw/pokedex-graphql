package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

// CheckCerts returns whether the required certs are in place
func checkCerts() bool {
	//useTLS := false
	hasCRT := false
	_, err := os.Stat("certs/tls.crt")
	if os.IsNotExist(err) {
		//log.Warn("Cannot load certs/tls.crt")
		log.Println("Cannot load certs/tls.crt")
	} else {
		hasCRT = true
	}

	hasKey := false
	_, err = os.Stat("certs/tls.key")
	if os.IsNotExist(err) {
		//log.Warn("Cannot load certs/tls.key")
		log.Println("Cannot load certs/tls.key")
	} else {
		hasKey = true
	}

	return hasCRT && hasKey
}

func loadConfiguration() {
	// Maybe have this override keys if missing
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
}
