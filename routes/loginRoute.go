package routes

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	log "github.com/sirupsen/logrus"
	database "gitlab.com/m2106lw/pokedex-graphql/database"
	"gitlab.com/m2106lw/pokedex-graphql/helpers"
	"gitlab.com/m2106lw/pokedex-graphql/types"
	"golang.org/x/crypto/bcrypt"
)

// NOTE: This may get replaced by an API Gateway of some sort

// TODO: Work on secret keys
func createCookie(token []byte) (*http.Cookie, error) {
	hashKey := []byte("hahahahahahahahahaahahah")
	blockKey := []byte("hahahahahahahahahaahahah")
	sCookie := securecookie.New(hashKey, blockKey)

	encoded, err := sCookie.Encode("token", token)
	if err != nil {
		log.Error(err.Error())
		return nil, err
	}

	// TODO: Work on modifying this based on HTTPS
	cookie := &http.Cookie{
		Name:     "token",
		Value:    encoded,
		Path:     "/",
		Secure:   false,
		HttpOnly: true,
	}
	return cookie, nil
}

// TODO: Work on secret key
func createJWT(userID uint) ([]byte, error) {
	hs := jwt.NewHS256([]byte("secret"))

	now := time.Now()
	// TODO: Add my own info
	payload := types.JWTPayload{
		Payload: jwt.Payload{
			Issuer:         "mwilliams",
			Subject:        "A user",
			Audience:       jwt.Audience{"https://golang.org", "https://jwt.io"},
			ExpirationTime: jwt.NumericDate(now.Add(24 * 30 * 12 * time.Hour)),
			NotBefore:      jwt.NumericDate(now.Add(30 * time.Minute)),
			IssuedAt:       jwt.NumericDate(now),
			JWTID:          "foobar",
		},
		UserID: userID,
	}

	token, err := jwt.Sign(payload, hs)
	return token, err
}

// This function is middleware to make sure an email or password was even sent in
func basicAuth(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// TODO: Add the right realm here
		w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
		w.Header().Set("Content-Type", "application/json")

		email, password, authOK := r.BasicAuth()
		if authOK == false {
			// TODO: Make this a json
			http.Error(w, "Missing email or password", 400)
			return
		}

		context.Set(r, "email", email)
		context.Set(r, "password", password)
		log.Println("Login attempt from", email)

		next.ServeHTTP(w, r)
	})
}

// This function is used for an existing user
func userLogin(w http.ResponseWriter, r *http.Request) {
	// Get the email and password from basicAuth
	email := context.Get(r, "email").(string)
	password := context.Get(r, "password").(string)

	// Check to see if user exists
	// TODO: Look into LDAP Auth at some point
	db := database.DB.Conn
	currentUser := database.User{}
	err := db.Where("email = ?", email).First(&currentUser).Error
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		helpers.ErrorAsJSON(w, "User not found or password incorrect")
		return
	}

	// If the user exists then compare the password they sent with the hash that is saved
	hashPassword := []byte(currentUser.Password)
	err = bcrypt.CompareHashAndPassword(hashPassword, []byte(password))
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		helpers.ErrorAsJSON(w, "User not found or password incorrect")
		return
	}

	// Create a jwt
	token, err := createJWT(currentUser.ID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}
	// Create a secure cookie
	cookie, err := createCookie(token)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}
	http.SetCookie(w, cookie)

	result := types.Result{Message: "Login Successful", Token: ""}
	// Reference: https://golang.org/pkg/net/http/
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)
}

// This function will create a new user
func createUser(w http.ResponseWriter, r *http.Request) {
	// Get the email and password from basicAuth
	email := context.Get(r, "email").(string)
	password := context.Get(r, "password").(string)

	// Create password hash with bcrypt
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}

	// Create a new user based on the info passed
	// TODO: Look into LDAP Auth at some point
	newUser := database.User{Email: email, Password: string(hashPassword)}
	db := database.DB.Conn
	err = db.Create(&newUser).Error
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}

	// Create a jwt
	token, err := createJWT(newUser.ID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}
	// Create a secure cookie
	cookie, err := createCookie(token)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		helpers.ErrorAsJSON(w, err.Error())
		return
	}
	http.SetCookie(w, cookie)

	// Send results
	result := types.Result{Message: "User has been created", Token: ""}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(result)
}

func loginHandler(r *mux.Router) {
	// Base path /login
	r.HandleFunc("/login", basicAuth(userLogin)).Methods("POST")

	// Anything inside of /login/
	loginRouter := r.PathPrefix("/login").Subrouter()
	loginRouter.HandleFunc("/users", basicAuth(createUser)).Methods("POST")
}
