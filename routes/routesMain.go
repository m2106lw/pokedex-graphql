package routes

import (
	"github.com/gorilla/mux"
)

// RouteHandler is the entry point for the REST api
func RouteHandler(r *mux.Router) {
	mainRouter := r.PathPrefix("/").Subrouter()

	statusHandler(mainRouter)
	loginHandler(mainRouter)
	//UsersHandler(mainRouter)
	//GroupsHandler(mainRouter)
	//ProjectsHandler(mainRouter)
}
