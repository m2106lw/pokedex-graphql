package routes

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type status struct {
	Status      string    `json:"status"`
	CurrentTime time.Time `json:"time"`
}

// Just return 200 if running
func getStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Return the time as well because idk
	returnData := &status{
		Status:      "running",
		CurrentTime: time.Now().UTC(),
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(returnData)
}

func statusHandler(r *mux.Router) {
	// Anything inside of /status/
	r.HandleFunc("/status", getStatus).Methods("GET")

	// TODO: Add more routes for checking stuff
}
