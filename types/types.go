package types

import (
	"github.com/gbrlsnchs/jwt/v3"
)

type Result struct {
	Token string `json:"token"`
	Message string `json:"message"`
}

type Error struct {
	Message string
}

type JWTPayload struct {
	jwt.Payload
	UserID uint `json:"user_id"`
}