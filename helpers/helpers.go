package helpers

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// GetUintID will take a key from the request and convert it to uint
func GetUintID(key string, r *http.Request) (uint, error) {
	id, ok := mux.Vars(r)[key]
	if !ok {
		err := errors.New("Missing group id")
		return 0, err
	}

	returnInt, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return 0, nil
	}

	return uint(returnInt), nil
}
