package helpers

import (
	"encoding/json"
	"net/http"
)

type errorJSON struct {
	Error string `json:"error"`
}

// ErrorAsJSON returns the error message as a json
func ErrorAsJSON(w http.ResponseWriter, errorInfo string) {
	returnError := &errorJSON{
		Error: errorInfo,
	}
	json.NewEncoder(w).Encode(returnError)
	return
}
